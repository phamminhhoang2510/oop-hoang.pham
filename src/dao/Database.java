package dao;


import entity.Accessotion;
import entity.Category;
import entity.Product;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Database {
    protected List<Product> productTable;

    protected List<Category> categoryTable;

    protected List<Accessotion> accessotionTable;

    protected Database instants;

    public List<Product> getProductTable() {
        return productTable;
    }

    public void setProductTable(List<Product> productTable) {
        this.productTable = productTable;
    }

    public List<Category> getCategoryTable() {
        return categoryTable;
    }

    public void setCategoryTable(List<Category> categoryTable) {
        this.categoryTable = categoryTable;
    }

    public List<Accessotion> getAccessotionTable() {
        return accessotionTable;
    }

    public void setAccessotionTable(List<Accessotion> accessotionTable) {
        this.accessotionTable = accessotionTable;
    }

    public Database getInstants() {
        return instants;
    }

    public void setInstants(Database instants) {
        this.instants = instants;
    }

    public int insertTable(String name, Object object) {
        if (object == null) {
            return 0;
        }

        if (name.equalsIgnoreCase("Product") && object instanceof Product) {
            productTable = new ArrayList<>();
            productTable.add((Product) object);
            System.out.println(productTable);
            return 1;
        }

        if (name.equalsIgnoreCase("Category") && object instanceof Category) {
            categoryTable = new ArrayList<>();
            categoryTable.add((Category) object);
            return 2;
        }

        if (name.equalsIgnoreCase("Accessotion") && object instanceof Accessotion) {
            accessotionTable = new ArrayList<>();
            accessotionTable.add((Accessotion) object);
            return 3;
        }
        return 0;
    }



    public List<Object> selectTable(String name) {
        if (name.equalsIgnoreCase("Product")) {
            return Collections.singletonList(productTable);
        } else if (name.equalsIgnoreCase("Category")) {
            return Collections.singletonList(categoryTable);
        } else if (name.equalsIgnoreCase("Accessotion")) {
            return Collections.singletonList(accessotionTable);
        }
        return null;
    }

    public int updateTable(String name, Object object) {


        return 0;
    }

    public boolean deleteTable(String name, Object object) {

        return true;
    }

    public void truncateTable(String name) {
        if (name.equalsIgnoreCase("Product")) {


        } else if (name.equalsIgnoreCase("Category")) {

        } else if (name.equalsIgnoreCase("Accessotion")) {

        }

    }


    public int updateTableById(int id, Object obj) {

        return id;
    }
}
