import dao.Database;
import entity.Product;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainApplication {
    public static void main(String[] args) {
        Database database = new Database();

        // insertTable
//        int kq = database.insertTable("Product",new Product(1,"Food",1));
//        if (kq==0)
//            System.out.println("Không đúng định dạng");
//        else System.out.println("thêm thành công");


        // selectTable
        List<Product> productTable = new ArrayList<>();
        productTable.add(new Product(1, "Food", 1));
        productTable.add(new Product(2, "Fish", 5));


//        database.truncateTable();
//        System.out.println(database);

    }
}
